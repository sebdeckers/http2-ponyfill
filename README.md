# http2-ponyfill

Implements the interface of Node.js' HTTP/2 native bindings for older versions of Node.js.

## Usage

```js
const http2 = require('http').HTTP2 || require('http2-ponyfill')

// http2: {
//   Http2Settings,
//   createClient,
//   createServer,
//   createSecureServer,
//   createServerSession,
//   createClientSession
// }
```

## Known Limitations

No idea yet. Just started this. 🤔

## Supported Node.js Versions

Versions refer to the official Node.js Docker tags.

- 0.12
- argon
- boron

## See also:

- [http2server](https://www.npmjs.com/package/http2server) — Uses this for optimal performance when possible, and backwards compatibility otherwise.
- [nodejs/http2](https://github.com/nodejs/http2) — Native bindings for Node.js
- [Ponyfill?](https://ponyfill.com/) — Pony pureness.
