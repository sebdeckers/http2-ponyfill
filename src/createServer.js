var spdy = require('spdy')

function createServer (options, callback) {
  if (typeof options === 'undefined') options = Object.create(null)
  if (typeof options.spdy === 'undefined') options.spdy = Object.create(null)
  options.spdy.protocols = ['h2']
  options.spdy.plain = true
  return spdy.createServer(options, callback)
}

module.exports = createServer
