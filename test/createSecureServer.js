import test from 'ava'
import {readFileSync} from 'fs'
import {parse} from 'url'
import http2 from 'http2'
import concat from 'concat-stream'
import createSecureServer from '../src/createSecureServer'

const key = readFileSync('test/fixtures/key.pem')
const cert = readFileSync('test/fixtures/cert.pem')

test('createSecureServer for HTTPS', (t) => {
  return new Promise((resolve, reject) => {
    const server = createSecureServer({key, cert}, (request, response) => {
      response.end('Got it!')
    })
    server.listen(0, 'localhost', () => {
      const {port} = server.address()
      const url = `https://localhost:${port}/`
      const options = parse(url)
      options.rejectUnauthorized = false
      const request = http2.get(options)
      const concatStream = concat({encoding: 'string'}, (body) => {
        if (body === 'Got it!') resolve()
        else reject()
      })
      request.on('error', reject)
      request.on('response', function (response) {
        response.pipe(concatStream)
        response.on('error', reject)
      })
    })
  })
})
