import test from 'ava'
import http2 from 'http2'
import concat from 'concat-stream'
import createServer from '../src/createServer'

test('createServer for HTTP', (t) => {
  return new Promise((resolve, reject) => {
    const server = createServer({}, (request, response) => {
      response.end('Got it!')
    })
    server.listen(0, 'localhost', () => {
      const {port} = server.address()
      const url = `http://localhost:${port}/`
      var request = http2.raw.get(url)
      var concatStream = concat({encoding: 'string'}, (body) => {
        if (body === 'Got it!') resolve()
        else reject()
      })
      request.on('error', reject)
      request.on('response', function (response) {
        response.pipe(concatStream)
        response.on('error', reject)
      })
    })
  })
})
