import test from 'ava'
import http2Ponyfill from '..'

test('interface', (t) => {
  const expected = [
    'Http2Settings',
    'createClient',
    'createServer',
    'createSecureServer',
    'createServerSession',
    'createClientSession'
  ]
  const actual = Object.keys(http2Ponyfill)
  t.deepEqual(actual, expected)
})
